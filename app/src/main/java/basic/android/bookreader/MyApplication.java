package basic.android.bookreader;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.orm.SugarApp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MyApplication extends SugarApp {

    private String DBPATH_DBNAME = "/data/data/basic.android.bookreader/databases/book_reader.db";
    private String DB_NAME = "book_reader.db";


    @Override
    public void onCreate() {
        super.onCreate();
        try {
            new DataBaseHelper(this);
        } catch (Exception e) {
        }


    }


    public class DataBaseHelper extends SQLiteOpenHelper {
        private Context mycontext;

        public SQLiteDatabase myDataBase;

        public DataBaseHelper(Context context) throws IOException {
            super(context, DB_NAME, null, 1);
            this.mycontext = context;
            boolean dbexist = checkdatabase();
            if (dbexist) {
                //System.out.println("Database exists");
                opendatabase();

            } else {
                System.out.println("Database doesn't exist");
                createdatabase();
            }
        }

        public void createdatabase() throws IOException {
            boolean dbexist = checkdatabase();
            if (dbexist) {
                //System.out.println(" Database exists.");
            } else {
                this.getReadableDatabase();
                try {
                    copydatabase();
                } catch (IOException e) {
                    throw new Error("Error copying database");
                }
            }
        }

        private boolean checkdatabase() {
            //SQLiteDatabase checkdb = null;
            boolean checkdb = false;
            try {

                File dbfile = new File(DBPATH_DBNAME);
                //checkdb = SQLiteDatabase.openDatabase(myPath,null,SQLiteDatabase.OPEN_READWRITE);
                checkdb = dbfile.exists();
            } catch (SQLiteException e) {
                System.out.println("Database doesn't exist");
            }
            return checkdb;
        }

        private void copydatabase() throws IOException {
            //Open your local db as the input stream
            InputStream myinput = mycontext.getAssets().open(DB_NAME);
            //Open the empty db as the output stream
            OutputStream myoutput = new FileOutputStream(DBPATH_DBNAME);
            // transfer byte to inputfile to outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myinput.read(buffer)) > 0) {
                myoutput.write(buffer, 0, length);
            }

            //Close the streams
            myoutput.flush();
            myoutput.close();
            myinput.close();
        }

        public void opendatabase() throws SQLException {
            //Open the database
            myDataBase = SQLiteDatabase.openDatabase(DBPATH_DBNAME, null, SQLiteDatabase.OPEN_READWRITE);
        }

        public synchronized void close() {
            if (myDataBase != null) {
                myDataBase.close();
            }
            super.close();
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {

        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }

    }
}
