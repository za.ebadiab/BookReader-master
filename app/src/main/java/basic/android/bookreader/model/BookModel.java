package basic.android.bookreader.model;

import com.orm.SugarRecord;

public class BookModel extends SugarRecord<BookModel> {

    private String onePageValue;

    public BookModel() {
    }

    public BookModel(String onePageValue) {
        this.onePageValue = onePageValue;
    }


    public String getOnePageValue() {
        return onePageValue;
    }

    public void setOnePageValue(String onePageValue) {
        this.onePageValue = onePageValue;
    }
}
