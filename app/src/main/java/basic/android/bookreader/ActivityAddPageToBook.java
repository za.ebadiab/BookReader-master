package basic.android.bookreader;

import android.content.Intent;
import android.os.Bundle;


import basic.android.bookreader.customViews.MyEditText;
import basic.android.bookreader.model.BookModel;

public class ActivityAddPageToBook extends BaseActivity {

    MyEditText pageContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_page_to_book);

        bind();
    }

    void bind() {
        pageContent = findViewById(R.id.pageContent);

        findViewById(R.id.savePage).setOnClickListener(v -> {
            savePageOnDB();
            Intent intent = new Intent(mContext, MainActivity.class);
            startActivity(intent);
        });
    }


    void savePageOnDB() {
        BookModel bookModel = new BookModel();
        bookModel.setOnePageValue(pageContent.text());

        bookModel.save();

    }
}
