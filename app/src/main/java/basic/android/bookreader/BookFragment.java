package basic.android.bookreader;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import basic.android.bookreader.customViews.MyTextView;
import basic.android.bookreader.model.BookModel;

public class BookFragment extends Fragment {

    MyTextView myTextView;

    private static BookFragment bookFragment;


    public static BookFragment getFragment(int position) {
        //    if (bookFragment == null) {
        bookFragment = new BookFragment();
        //  }

        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bookFragment.setArguments(bundle);

        return bookFragment;

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.book_fragment_item, container, false);
        myTextView = v.findViewById(R.id.txtPageContent);

        Log.d("my log", "position= " + getArguments().getInt("position", 0));
        String value = getPageFromDB(getArguments().getInt("position", 0));
        myTextView.setText(value);

        return v;
    }


    public String getPageFromDB(long i) {

        Log.d("my log", "in getPageFromDB");

        BookModel bookModel = BookModel.findById(BookModel.class, i + 1);

        if (bookModel != null) {
            return bookModel.getOnePageValue();
        }
        return "";

    }


}
