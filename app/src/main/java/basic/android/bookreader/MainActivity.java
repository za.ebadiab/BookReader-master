package basic.android.bookreader;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import karacken.curl.PageCurlAdapter;
import karacken.curl.PageSurfaceView;

public class MainActivity extends BaseActivity {

    ViewPager pager;
    SmartTabLayout viewpagertabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
        setPagerAdapter();

        setTabs();

    }




    void bind() {

        pager = findViewById(R.id.pager);
        viewpagertabs = findViewById(R.id.viewpagertabs);

     /*   findViewById(R.id.btnAddpage).setOnClickListener(v -> {
            Intent intent = new Intent(mContext, ActivityAddPageToBook.class);
            startActivity(intent);

        });
*/
    }


    void setPagerAdapter() {
        BookAdapter bookAdapter = new BookAdapter(getSupportFragmentManager());
        pager.setAdapter(bookAdapter);
        pager.setOffscreenPageLimit(3);

    }
    void setTabs(){
        viewpagertabs.setViewPager(pager);

    }
/*
    void setBookPageCurl(){
        PageSurfaceView pageSurfaceView = new PageSurfaceView(this);
        String[] asset_res_array=null;
        asset_res_array=  new String[]{"page1.png", "page2.png", "page3.png"};
        PageCurlAdapter pageCurlAdapter=new PageCurlAdapter(asset_res_array);
        pageSurfaceView.setPageCurlAdapter(pageCurlAdapter);
        setContentView(pageSurfaceView);
    }
*/
}
